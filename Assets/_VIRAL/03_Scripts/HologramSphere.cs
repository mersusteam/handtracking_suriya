﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _VIRAL._03_Scripts
{
    public class HologramSphere : MonoBehaviour
    {
        private ParentBlackSphere hologramSphere;
        private GameObject _child;
        private RoboticArm _rArm;
       private RobotHologram robot;
        // Start is called before the first frame update
        void Start()
        {
            _child = GameObject.Find(gameObject.name);
            _rArm = GameObject.FindObjectOfType<RoboticArm>();
            robot = GameObject.FindObjectOfType<RobotHologram>();
        }

        // Update is called once per frame
        void Update()
        {
            if (_child)
            {
               gameObject.transform.localPosition = _child.transform.position * 0.0642f;
            }
            
        }

        private void OnCollisionEnter(Collision collision)
        {
            Debug.Log("Collision Out" +collision.gameObject.layer);

            if (collision.gameObject.layer == 9)
            {
                // Debug.Log("Collision");
                robot.GetRelativeTargetPos(gameObject.transform.position);
                _rArm.AbortAndSetNewTarget(_child.GetComponent<Holdable>());
                //// _hit = true;
            }
        }
        
    }
}
