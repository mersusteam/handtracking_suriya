﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _VIRAL._03_Scripts
{
    public class ButtonSpawner : MonoBehaviour
    {
        [SerializeField] private Spawner _spawner;
        [SerializeField] private ParticleSystem _redPlop;
        private bool _hit = false;
        // Start is called before the first frame update
        void Start()
        {

        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log("Hitting in Trigger");
            
                CallSpawner();
                //_hit = true;
            
        }

        private void OnCollisionEnter(Collision collision)
        {
            Debug.Log("Hitting in Collision");
        }
        private void CallSpawner()
        {
            Debug.Log("Inside Spawner");
            _redPlop.Play();
            _spawner.Spawn();

            
        }

        // Update is called once per frame
        void Update()
        {
            if (_redPlop.isStopped)
            {
               // _hit = false;
            }

        }
    }
}