﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _VIRAL._03_Scripts
{
    public class HologramSpawner : MonoBehaviour
    {
       
        [SerializeField] private GameObject _toBeSpawned;
        public  List<GameObject> _hologramSpheres = new List<GameObject>();
        private List<GameObject> _refSpheres = new List<GameObject>();
        
        // Start is called before the first frame update
        void Start()
        {

           
        }

        // Update is called once per frame
        void Update()
        {
           
        }

        public void Spawn(Transform _newObjectAdded)
        {
            GameObject _new = Instantiate(_toBeSpawned, this.gameObject.transform);
            _new.transform.localPosition = new Vector3(_newObjectAdded.transform.position.x*0.0642f, _newObjectAdded.transform.position.y * 0.0642f, _newObjectAdded.transform.position.z * 0.0642f);
            _new.gameObject.name = _newObjectAdded.gameObject.name;
            _hologramSpheres.Add(_new);
            _refSpheres.Add(_newObjectAdded.gameObject);

            _hologramSpheres = _refSpheres;

        }


    }
}
